package com.example.credit.product.controller;

import com.example.credit.product.model.Product;
import com.example.credit.product.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping ("/clients")
    public ResponseEntity getAllProducts() {
        return ResponseEntity.ok(this.productRepository.findAll());
    }

    @GetMapping("/client/{id}")
    public ResponseEntity getClient(@PathVariable Long id) {
        return ResponseEntity.ok(this.productRepository.findById(id));
    }

    @PostMapping("/client/")
    public ResponseEntity createClient(@RequestBody Product product) {
        return ResponseEntity.status(201).body(this.productRepository.save(product));
    }

    @PutMapping("/client/{id}")
    public ResponseEntity updateClient(@PathVariable Long id, @RequestBody Product product) {
        Product productToUpdate = this.productRepository.findById(id).get();
        productToUpdate.setName(product.getName());
        productToUpdate.setSurname(product.getSurname());
        productToUpdate.setAge(product.getAge());
        productToUpdate.setSum(product.getSum());
        productToUpdate.setResult(product.getResult());
        return ResponseEntity.ok(this.productRepository.save(productToUpdate));
    }

    @DeleteMapping("/client/{id}")
    public ResponseEntity deleteClient(@PathVariable Long id) {
        Product product = this.productRepository.findById(id).get();
        this.productRepository.deleteById(id);
        return ResponseEntity.ok(product);
    }
}

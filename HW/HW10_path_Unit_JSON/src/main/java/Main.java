import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class Main {
    public static void main(String[] args) throws IOException, JSONException {
        listCredit();

    }

    public static ArrayList<String> listCredit() {
        //read JSONobject from file test.json
        JSONParser parser = new JSONParser();
        org.json.simple.JSONObject data = null;
        try {
            data = (org.json.simple.JSONObject) parser.parse(
                   
				   //!!!Path to /src/main/resources/test.json
            new FileReader("../HW10_JSON/src/main/resources/test.json")); //!!!Path to /src/main/resources/test.json
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String json = data.toJSONString();
        JSONObject jsonObj = new JSONObject(json);
        JSONArray jsonPeople = jsonObj.getJSONArray("people");
        String name = null;
        Integer age = null;
        Integer amount = null;
        ArrayList<String> client = new ArrayList<String>();
        for (int i = 0; i < jsonPeople.length(); i++) {
            name = jsonPeople.getJSONObject(i).getString("name");
            age = jsonPeople.getJSONObject(i).getInt("age");
            amount = jsonPeople.getJSONObject(i).getInt("sum");
            if (!Objects.equals(name, "Bob") && age > 18 && amount <= age * 100) {
                client.add(name);
                System.out.println("Кредит одобрен: " + name + " " + age + " " + amount);
            }
        }
        return client;
    }
}
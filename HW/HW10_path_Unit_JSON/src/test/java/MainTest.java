import junit.framework.TestCase;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class MainTest extends TestCase {

    public void testMain() {
    }

    @Test
    public void testListCredit() {
        ArrayList<String> jsonTest = Main.listCredit();
        Assertions.assertNotEquals(jsonTest.contains("Bob"), jsonTest, "Bob in black list. No credit!");
    }
}
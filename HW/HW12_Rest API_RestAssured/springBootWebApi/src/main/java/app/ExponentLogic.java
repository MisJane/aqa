package app;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

public class ExponentLogic {
    public static JSONObject exponentNumber(int number){
        JSONObject jsonObject = new JSONObject();
        for(int i = 1; i<5; i++){
            int result = (int) Math.pow(number, i);
            jsonObject.put(String.valueOf(i), String.valueOf(result));
        }
        return jsonObject;
    }

}
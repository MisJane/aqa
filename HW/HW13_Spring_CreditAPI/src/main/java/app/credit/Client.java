package app.credit;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class Client {
    public String name;
    public Integer age;
    public Integer sum;
    public UUID id;

    public void saveClient(String path, ObjectMapper mapper) throws IOException {
        mapper.writeValue(new File(path), this);
    }

}

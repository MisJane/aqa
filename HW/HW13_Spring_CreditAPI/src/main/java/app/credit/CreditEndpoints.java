package app.credit;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class CreditEndpoints {
    public static String soursePath = "../src/main/resources/resources";  //!!! SET path to resources (dir+resourses)
    private ObjectMapper mapper = new ObjectMapper();

    @PostMapping(path = "/client", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Client createClient(@RequestBody TempClient temp) throws IOException {
        Client client = temp.toClient();
        client.saveClient(soursePath + client.id, mapper);
        return client;
    }

    @GetMapping(path = "/client/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getClient(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(soursePath + id);
        return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
    }

    private String resultCredit(String name, int age, int sum) {
        return (age >= 18) && (!name.equals("Bob")) && (sum < age * 1000) ? "OK"
                : "NO credit";
    }

    @GetMapping(path = "/getResult/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getResult(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(soursePath + id);
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for (int i = 0; i < params.length; i++) {
            if (params[i].contains("age")) {
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if (params[i].contains("sum")) {
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if (params[i].contains("name")) {
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return resultCredit(name, age, sum);
    }

    @DeleteMapping(path = "/client/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteClient(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(soursePath + id);
        return Files.deleteIfExists(path);
    }

    // другим способом инфо о клиенте
    @RequestMapping(
            path = "/client/{id}",
            method = RequestMethod.GET
    )
    @Operation(summary = "Get сlient's info")
    public String getClient1(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(soursePath + id);
        return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
    }

/*     @RequestMapping(
            path = "/clients",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Get info about all clients")
    @ResponseBody
   public String getAllClients(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get(soursePath + id);
        return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
    }
    //попытка получить список клиентов
    */

}

package app.credit;

import java.util.UUID;

public class TempClient {
    public String name;
    public Integer age;
    public Integer sum;

    public Client toClient() {
        Client tempClient = new Client();
        tempClient.age = age;
        tempClient.name = name;
        tempClient.sum = sum;
        UUID uniqueKey = UUID.randomUUID();
        tempClient.id = uniqueKey;
        return tempClient;
    }
}

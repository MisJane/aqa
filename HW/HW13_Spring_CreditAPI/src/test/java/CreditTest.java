import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static app.credit.CreditEndpoints.soursePath;
import static io.restassured.RestAssured.given;

public class CreditTest {
    @DataProvider
    private Object[][] users(){
        return new Object[][]{
                {"Vova", 40, 500},
                {"Sahsa", 50, 20},
                {"Elena", 15, 300},
                {"Oleg", 20, 1800},
        };
    }

    @Test
    public void createClient(){
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("name", "Test");
        bodyJson.put("age", 30);
        bodyJson.put("sum", 3000);

        Response response = given().contentType(ContentType.JSON).body(bodyJson.toString()).post("http://localhost:8080/client")
                .then().log().all().extract().response();
        JsonPath jsonPath = response.jsonPath();
        Assert.assertNull(jsonPath.getJsonObject("id"));
    }

    @Test
    public void deleteClient(){
        given().delete("http://localhost:8080/client").then().log().all()
                .assertThat().statusCode(200);
    }

    @Test
    public void chekRes() throws IOException {
        System.out.println(getResult());
    }
    private String getResult() throws IOException {
        String url = soursePath+"123";
        Path path = Paths.get(soursePath + "123");
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  (age >= 18) && !(name.equals("Bob")) && (sum<= age * 100) ? "OK credit" : "NO credit";
    }
}

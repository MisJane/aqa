import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        System.out.println("выдает случайное число от 1970 до текущего года:");
        System.out.println(TestHelper.getRandomYear());
        System.out.println('\n' + "выдает случайное число типа long:");
        System.out.println(TestHelper.getRandomLong());
        System.out.println("\nгенерирует случайную строку, 3 слова с большой буквы:");
        System.out.println(TestHelper.getRandomString());
        System.out.println('\n' + "прочитает файл src/main/java/123.txt из нескольких строк и составит из них HashMap:");
        System.out.println(Collections.unmodifiableMap(TestHelper.getHashMapFromFile("src/main/java/123.txt", "::")));
        System.out.println("\nпреобразует Unix формат даты 1643297846 в другой:");
        System.out.println(TestHelper.formatDate(1643297846));
        System.out.println('\n' + "превращает строку 3865736 в число Double и, если это невозможно, возвращает Infinity:");
        System.out.println(TestHelper.stringToDouble("3865736"));

    }
}

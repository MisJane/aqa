import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

class TestHelper {
    private static int randomYear() {
        final Random random = new Random();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int randYear = random.nextInt(currentYear - 1970) + 1970;
        return randYear;
    }

    public static int getRandomYear() {
        return randomYear();
    }

    private static long randomLong() {
        Random random = new Random();
        return random.nextLong();
    }

    public static long getRandomLong() {
        return randomLong();
    }


    private static String randomString() {
        final Random random = new Random();
        StringBuilder sb = new StringBuilder();
        int maxWordLength = 5;

        for (int i = 0; i < 3; i++) {
            int wordLength = random.nextInt(maxWordLength) + 1;
            sb.append((char) Math.round(Math.random() * 25 + 65));
            for (int j = 1; j < wordLength; j++)
                sb.append((char) Math.round(Math.random() * 25 + 97));
            sb.append(' ');
        }
        return sb.toString().strip();
    }

    public static String getRandomString() {
        return randomString();
    }

    public static HashMap<String, String> getHashMapFromFile(String filePath, String splitter) {
        HashMap<String, String> result = new HashMap<>();

        try {
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            String line = reader.readLine();
            while (line != null) {
                String[] keyValue = line.split(splitter);
                result.put(keyValue[0].strip(), keyValue[1].strip());

                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String formatDate(long unixDateSeconds) {
        Date date = new Date(unixDateSeconds * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd z");

        return sdf.format(date);
    }

    public static double stringToDouble(String stringNum) {
        try {
            return Double.parseDouble(stringNum);
        } catch (NumberFormatException nfe) {
            return Double.POSITIVE_INFINITY;
        }
    }
}
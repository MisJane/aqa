import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Calendar;

class TestHelperTest {

    @Test
    void getRandomYear() {
        Assert.assertTrue(TestHelper.getRandomYear() >= 1970);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Assert.assertTrue(TestHelper.getRandomYear() <= year);
    }

    @Test
    void getRandomLong() {
        long value = TestHelper.getRandomLong();
        Assert.assertTrue(String.valueOf(value).length() > 10);
    }

    @Test
    void getRandomString() {
        String whiteSpace = TestHelper.getRandomString();
        whiteSpace = whiteSpace.strip();
        String[] testSplit = whiteSpace.split(" ");
        Assert.assertTrue(testSplit.length > 2);
    }

    @Test
    void getHashMapFromFile() {
        File file = new File("src/main/java/123.txt");
        Assert.assertTrue(file.length() > 0);
    }

    @Test
    void formatDate() {
        String unixNullDate = TestHelper.formatDate(0);
        Assert.assertTrue(unixNullDate.contains("1970-01-01"));
    }

    @Test
    void stringToDouble() {
        Double infinity = TestHelper.stringToDouble("ooo");
        String strInf = infinity.toString();
        Assert.assertTrue(strInf.contains("Infinity"));

        Double infinityNum = TestHelper.stringToDouble("000");
        Assert.assertTrue(infinityNum == 0.00);
    }
}
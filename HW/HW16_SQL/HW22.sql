--Найти самый дорогой товар, вывести имя и цену
select name as Goods, max(value) as MaxPrice from Goods G join Prices P on G.id = P.goods_id;

--Найти товары с нулевым остатком. Вывести имя и цену
select name as Goods, value as Value from Quantity Q join Goods G on G.id = Q.goods_id where value = 0;

--Найти производителя с самой большой ценой за товар. Вывести имя производителя и стоимость.
select M."name" as Manufacturer, max("value") as MaxPrice from Manufacturer M join Suppliers S on M.id = S.manufacturer_id join Goods G on S.id = G.supplier_id join Prices P on G.id = P.goods_id;

--Найти все товары производителей Москвы. Вывести имена товаров, цены, имена производителей.
select G.name as Goods, value as Price, Manufacturer.name as Manufacturer from Manufacturer join Suppliers S on Manufacturer.id = S.manufacturer_id join Goods G on S.id = G.supplier_id join Prices P on G.id = P.goods_id where location like 'Moscow';

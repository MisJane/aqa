package com.webVillage.app.webControllers;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class GreetingControllerTest {

    private WebDriver driver;
    private String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\PROJects\\Java\\Less\\webFun_lec25\\src\\main\\resources\\chromedriver.exe");
    }

    @BeforeEach
    public void beforeTest() {
        driver = new ChromeDriver();
        driver.get(startPage + port + "/greeting");
    }

    @AfterEach
    public void afterTest() {
        driver.quit();
    }

    @Test
    void greetingSearchFirstElementH3() {
        WebElement element = driver.findElement(By.xpath("//h3"));
        String text = element.getText();
        Assertions.assertEquals("Hello, World!", text);
    }

    @ParameterizedTest
    @CsvSource(value = {"//h3;;Hello, World!"}, delimiterString = ";;")
    void greetingSearchFirstH3Another(String xpath, String expectedResult) {
        WebElement element = driver.findElement(By.xpath(xpath));
        String text = element.getText();
        Assertions.assertEquals(expectedResult, text);
    }

    @Test
    void greetingSearchFirstElementsH3() {
        List<WebElement> element = driver.findElements(By.xpath("//h3"));
        String text = element.toString();
        System.out.println(driver.getPageSource());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "//h3;;Hello, World!",
            "//h3[2];;Find me!",
            "//h3[@class='header'];;And me!",
            "//h3[@class='header'][2];;NOW me!",
            "//h3[@class='header yellow'];;Do not forget about me",
            "//h3[@class='header'][@thestyle='red'];;It is simple",
            "//h3[@class='header'][starts-with(@thestyle,'r')];;It is simple",
            "//h3[@class='header'][contains(@thestyle,'e')];;It is simple",
            "//h3[@class='header'][ends-with(@thestyle,'ed')];;It is simple",
         //с окончанием на 'd' не отрабатывает :(
         //   "//h3[@class='header'][ends-with(@thestyle,'d')];;It is simple",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'kitty-cat')];;My turn",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'bow-wow-dog')];;Where am I...",
            "//h3[not(contains(@class, 'abracadabra'))][contains(@class, 'kitty-cat')];;What about me",
            "//div[@id='good']/h3;;I am hiding",
            "//div[@id='bad']/h3;;U cannot get me!",
            "//div[@id='ugly']//h3;;Where is Blondie again?",
            "//p[@id='not_me'];;Find my daddy please.",
            "//p[@id='iAmLost']/ancestor::h3;;Look for my granny!",
            "//h3[@id='sister'];;Sister",
            "//h3[@id='sister']/../..//h3[not(@id='sister')];;Brother",
            "//h3[@class='two'];;Please",
            "//h3[@class='three'];;Find us",
            "//h3[@class='four'];;At once",
            "//div[@id='last']//h3[last()];;And me, please",
            "//div[@id='last']//h3[last()-1];;Find me!",
            "//h3[@price!=5];;Find me because my price is not 5",
            "//h3[@age>4];;Find me because my age is bigger than 4",
            "//h3[@courage>5 and @courage<15];;Find me because my courage is between 5 and 15",
            "//h3[text()='U can find me by text'];;U can find me by text",
            "//h3[contains(text(),'part of the text')];;U can find me by the part of the text",
           //не разобралась как в данном случае сделать чтобы не включен был текст 'text' :(
           //если исключить только слово text, то найтись должны многие строки, не уникальный параметр поиска..
            // "//h3[not(contains(text(),'text'))];;U can find me by the absence of word t e x t",
            "//h3[contains(text(),'absence of word')];;U can find me by the absence of word t e x t",
            "//h3[@id='pretender'];;Pretend that I'm constantly changing my tag. And find me"
    },
            delimiterString = ";;")
    void greeting(String xpath, String expectedResult) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        String text = element.getText();
        System.out.println(driver.getPageSource());
        Assertions.assertEquals(expectedResult, text);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "h3;;Hello, World!",
            "h3:nth-child(2);;Find me!",
            "h3.header;;And me!",
            "h3.header:nth-child(4);;NOW me!",
            "h3.header.yellow;;Do not forget about me",
            "h3.header[thestyle*='ed'];;It is simple",
            "h3.kitty-cat.abracadabra;;My turn",
            "h3.bow-wow-dog;;Where am I...",
            "h3.kitty-cat:not(.abracadabra);;What about me",
            "div#good h3;;I am hiding",
            "div#bad h3;;U cannot get me!",
            "div#ugly h3;;Where is Blondie again?",
            "p#not_me;;Find my daddy please.",
            "p#iAmLost;;Look for my granny!",
            "h3#sister;;Sister",
            //В css нельзя вернуться к родительскому чтобы найти id не sister
            //"h3[@id='sister']/../..//h3[not(@id='sister')];;Brother",
            "h3.two;;Please",
            "h3.three;;Find us",
            "h3.four;;At once",
            "div#last h3:last-child;;And me, please",
            "div#last h3:nth-last-child(2);;Find me!",
            "h3[price]:not([price='5']);;Find me because my price is not 5",
            "h3[age]:not([age='4']);;Find me because my age is bigger than 4",
          // "h3:contains("text")"
    },
            delimiterString = ";;")
    void greetingCss(String css, String expectedResult) throws InterruptedException {
        WebElement element = driver.findElement(By.cssSelector(css));
        String text = element.getText();
        System.out.println(driver.getPageSource());
        Assertions.assertEquals(expectedResult, text);
    }
}
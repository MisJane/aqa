package com.webVillage.app.webControllers;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

// https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/

//https://chromedriver.chromium.org/downloads

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class IndexControllerTest {

    private WebDriver driver;
    private String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\PROJects\\Java\\Less\\webFun_lec25\\src\\main\\resources\\chromedriver.exe");
    }

    @BeforeEach
    public void beforeTest(){
          driver = new ChromeDriver();
    }

    @AfterEach
    public void afterTest(){
        driver.quit();
    }

    @Test
    void greeting() throws InterruptedException {

        driver.get(startPage +  port +  "/person/new");
        driver.findElement(By.id("firstName")).sendKeys("Alisa");
        driver.findElement(By.id("secondName")).sendKeys("Skrynko");
        driver.findElement(By.id("age")).sendKeys("18");

        WebElement element = driver.findElement(By.id("age"));
        Assertions.assertTrue(element.isDisplayed());  //элемент виден
        Assertions.assertTrue(element.isEnabled());    //элемент активен
        Thread.sleep(5000);

        element.sendKeys(Keys.CONTROL + "A");
        element.sendKeys(Keys.DELETE);  //equals element.clear();

        Thread.sleep(5000);

        List<WebElement> occupation = driver.findElements(By.id("occupation"));
        Random rnd = new Random();
        occupation.get(rnd.nextInt(occupation.size())).click();

        driver.findElement(By.cssSelector("select#gender"))
                .findElement(By.cssSelector("option[value='FEMALE']")).click();

        driver.findElement(By.id("doIt")).click();
      //  Thread.sleep(5000);
        assertEquals(startPage + port + "/", driver.getCurrentUrl());
    //    Thread.sleep(5000);
        System.out.println(driver.getPageSource());

    }



}
package com.webVillage.app.webControllers;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.time.Duration;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

import org.openqa.selenium.*;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;


// https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/

//https://chromedriver.chromium.org/downloads

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class IndexControllerTest {

    private WebDriver driver;
    private final String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\PROJects\\Java\\Less\\webFun_lec25\\src\\main\\resources\\chromedriver.exe");
    }

    @BeforeEach
    public void beforeTest(){
          driver = new ChromeDriver();
    }

    @AfterEach
    public void afterTest(){
        driver.quit();
    }

    @Test
    void greeting() throws InterruptedException {
        String name = "Alisa";
        String surname = "Skrynko";

        //каждый раз когда драйвер ищет элементы чтобы искал в течении заданного времени:
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        driver.get(startPage +  port +  "/person/new");
        driver.findElement(By.id("firstName")).sendKeys(name);
        driver.findElement(By.id("secondName")).sendKeys(surname);
        driver.findElement(By.id("age")).sendKeys("18");

        List<WebElement> occupation = driver.findElements(By.id("occupation"));
        Random rnd = new Random();
        occupation.get(rnd.nextInt(occupation.size())).click();
        assertEquals(startPage + port + "/", driver.getCurrentUrl());
        System.out.println(driver.getPageSource());

        driver.findElement(By.cssSelector("select#gender"))
                .findElement(By.cssSelector("option[value='FEMALE']")).click();

        driver.findElement(By.id("doIt")).click();

        //Ожидания
        //Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //WebElement element = driver.findElement(By.cssSelector("#nameLoaded"));

/*
        boolean elementIsVisible = wait.until(ExpectedConditions.textToBe(
                By.cssSelector("#name"), "NAME: " + name + " " + surname));
*/

        /*  WebElement loading = driver.findElement(By.cssSelector("#loadingForm"));
        wait.until(ExpectedConditions.and(ExpectedConditions.stalenessOf(loading),
                ExpectedConditions.not(
                        ExpectedConditions.textToBe(By.cssSelector("#name"), ""))));*/
        Wait<WebDriver> waitFluent = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(NoSuchElementException.class);

        WebElement element = waitFluent.until(new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return driver.findElement(By.cssSelector("#nameLoaded"));
            }
        });

        //Assertions.assertTrue(elementIsVisible);
        Assertions.assertEquals("NAME: " + name + " " + surname, element.getText());

        //assertEquals(startPage + port + "/", driver.getCurrentUrl());

        System.out.println(driver.getPageSource());


    /*    WebElement element = driver.findElement(By.id("age"));
        Assertions.assertTrue(element.isDisplayed());  //элемент виден
        Assertions.assertTrue(element.isEnabled());    //элемент активен
        Thread.sleep(5000);

        element.sendKeys(Keys.CONTROL + "A");
        element.sendKeys(Keys.DELETE);  //equals element.clear();

        Thread.sleep(5000);*/

    }

    @Test
    void iFrameTesting() throws InterruptedException {
        driver.get(startPage + port + "/");
        driver.switchTo().frame("lol");
        WebElement element = driver.findElement(
                By.xpath("//h1[@class='text-center text-white']"));
        System.out.println(element.getText());
        driver.switchTo().parentFrame();
    }

    @Test
    void jsTesting() throws InterruptedException {
        driver.get(startPage + port + "/person/new");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //js.executeScript("document.body.innerHTML = '<h2>GFKKHGVHVK</h2>'");
/*        String str = "HELLO";
        js.executeScript("document.body.innerHTML = '<h2>' + arguments[0] + arguments[1] + '</h2>'",
                str, "!!!!!!!");*/
        WebElement namejs = driver.findElement(By.id("firstName"));
        Thread.sleep(1000);
        js.executeScript("arguments[0].value = 'NAME FROM JS!!!'", namejs);

        String value = (String) js.executeScript("return arguments[0].value", namejs);
        System.out.println("VALUE: " + value);

        WebElement parent = (WebElement) js.executeScript("return arguments[0].parentElement", namejs);
        System.out.println("Parent: " + parent.getTagName());

        WebElement surnamejs = driver.findElement(By.id("secondName"));
        Thread.sleep(1000);
        js.executeScript("arguments[0].value = 'SURNAME FROM JS!!!'", surnamejs);

        String valuesname = (String) js.executeScript("return arguments[0].value", surnamejs);
        System.out.println("VALUE: " + valuesname);

        WebElement parentsname = (WebElement) js.executeScript("return arguments[0].parentElement", surnamejs);
        System.out.println("Parent: " + parentsname.getTagName());
/*
        driver.manage().window().setSize(new Dimension(300, 300));
        Thread.sleep(5000);

        js.executeScript("arguments[0].scrollIntoView(); \n window.scrollBy(0, -50)", element);
        */

        //js.executeScript("window.location.replace('http://google.com')");
        // Thread.sleep(5000);

    }

}
package com.company;
import java.util.Scanner;

public class HW3_Credit {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input your name: ");
        String name = in.nextLine();
        System.out.print("Input your age: ");
        int age = in.nextInt();
        System.out.print("Input the loan amount: ");
        double amount = in.nextDouble();
        double MaxAmount = age * 100;

        if (name.equals("Bob")) {System.out.println("Bob in black list");}
        else if (age < 18) {
            System.out.println("До 18 лет кредит не выдаём");
        } else if (amount < 0) {
            System.out.println("Введите корректную сумму кредита");}
        //тут надо было через while, но я пока не разобралась в этом
        else if (amount <= MaxAmount) {
            System.out.println("Ура! Кредит выдан");
        } else {
            System.out.println("Слишком большая сумма кредита. Максимально возможный кредит " + MaxAmount);
        }

        System.out.printf("Name: %s  Age: %d  Amount: %.2f Maximum amount: %.2f \n", name, age, amount, MaxAmount);

        in.close();
    }
}


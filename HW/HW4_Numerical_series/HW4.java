//package HW;

import java.util.Scanner;

public class HW4 {
    static int first(int a) {
        return a + 1;
    }

    static int second(int a) {
        return 2 * a;
    }

    public static void main(String[] args) {
        System.out.println("Продолжите числовой ряд:");
        int n = 5;
        Scanner input = new Scanner(System.in);
        int number;
        int a;

        for (int i = 0; i < 3; i++) {
            a = 2;
            for (int j = 0; j < n; j++) {
                System.out.print(a);
                System.out.print(" ");

                if (j % 2 == 0) {
                    a = first(a);
                } else {
                    a = second(a);
                }
            }

            System.out.println("?");
            number = input.nextInt();
            if (number == a) {
                System.out.println("Это верное число!");
                break;
            } else {
                System.out.println("Попробуй ещё");
            }
        }
    }
}

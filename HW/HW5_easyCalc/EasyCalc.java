import java.util.Scanner;

public class EasyCalc {
    public static void main(String[] args) {
        System.out.println("Enter the number, the operation and the second number, please:");
        Scanner input = new Scanner(System.in);
        String operation = input.next();
        operation = operation.replaceAll(" ", "");
        if(!operation.matches("[0-9]*[*/+-][0-9]*")){
            System.out.println("The operation is impossible");
            System.exit(0);
        }

        String[] numbers = operation.split("[*/+-]");
        String action = operation.replaceAll("[0-9]", "");
        double result = 0;
        switch (action){
            case "*":
                result = Double.parseDouble(numbers[0])*Double.parseDouble(numbers[1]);
                break;
            case "/":
                result = Double.parseDouble(numbers[0])/Double.parseDouble(numbers[1]);
                break;
            case "+":
                result = Double.parseDouble(numbers[0])+Double.parseDouble(numbers[1]);
                break;
            case "-":
                result = Double.parseDouble(numbers[0])-Double.parseDouble(numbers[1]);
                break;
        }
        System.out.println(result);

    }
}

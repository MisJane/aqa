import java.util.Scanner;

import static Main.scan;

public class Decoder {
    String result = "";
    void decode() {
        System.out.println("Введите текст для раскодирования: ");
        Scanner scan = new Scanner(System.in);
        String inputValue = scan.nextLine();
        System.out.println("Введите числовой ключ для кодирования: ");
        int inputKey = scan.nextInt();

        for (int i = 0; i < inputValue.length(); i++) {
            char ch = inputValue.charAt(i);
            ch = (char) (ch - inputKey);
            result = result + ch;
        }
        System.out.println("Текст после раскодирования: " + result);
    }

}

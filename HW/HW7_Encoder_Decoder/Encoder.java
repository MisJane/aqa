import java.util.Scanner;

import static Main.scan;

public class Encoder {
    String result = "";
    void code() {
        System.out.println("Введите текст для кодирования: ");
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();

        System.out.println("Введите числовой ключ для кодирования: ");
        int inputKey = scan.nextInt();

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            ch = (char) (ch + inputKey);
            result = result + ch;
        }
        System.out.println("Текст после кодирования: " + result);
    }

}

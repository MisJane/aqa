import java.util.Scanner;

public class Main {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Закодировать текст нажмите - 1, Раскодировать текст нажмите - 2");
        Encoder codeString = new Encoder();
        Decoder decodeString = new Decoder();
        try {
            int mode = scan.nextInt();
            if (mode == 1) {
                codeString.code();
            } else if (mode == 2) {
                decodeString.decode();
            } else {
                System.out.println("Вы не выбрали режим программы");
            }
        } catch (Exception ex) {
            System.out.println("Не верный формат данных");
        }
    }
}
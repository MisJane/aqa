package Village;

public abstract class Citizen {
    protected String firstName;
    protected String lastName;
    protected Race race;

    @Override
    public String toString() {
        return firstName + " " + lastName + " (" + race + ") ";
    }

    public Race getRace() {
        return race;
    }
    public void setRace(Race race) {
        this.race = race;
    }

}

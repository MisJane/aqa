HW8_Village

Написать приложение про историю деревни с 1650 по 1750 год:

Должен быть экземпляр класса Village, в котором тем или иным способом хранятся данные жителях (до 50 штук)
Жители принадлежат к следующим классам

Peasant
Witch
Vampire
Werewolf

У каждого жителя деревни должно быть уникальное имя и фамилия.
История деревни состоит из записей о событиях.
История формируется циклом, перебирающим каждый день и генерирующим 1 событие.
Каждый день должно произойти одно из четырех событий:

Житель приезжает в деревню (добавляется в Village)
Житель уезжает из деревни (убирается из Village)
Житель идет в гости к другому жителю
Все спят. Ничего не происходит.

Встреча в гостях приводит к следующим последствиям
Любой житель (кроме Witch) встречается с Witch в пятницу 13-е или 31 октября.

Вызывается MurderException и житель исчезает из деревни.

Любой житель встречается с Vampire

Если житель Peasant или Witch 5% шанс, что житель становится Vampire
5% Вызывается MurderException и житель исчезает из деревни.
1% Вызывается DawnException и Vampire исчезает из деревни.

Любой житель встречается с Werewolf в полнолуние

Вызывается MurderException и житель исчезает из деревни.

В остальных случаях происходит просто чаепитие.

Вывод будет примерно такой
 11 Apr. 1661 (Wednesday) Joan Hickson (Peasant) drinks tea with Karl Brandon (Vampire)
 12 Apr. 1661 (Thursday) Everybody sleeps. Nothing is happening
 13 Apr. 1661 (Friday) MURDEREXCEPTION Mike Longhead (Peasant) killed by Joe Dow (Witch)
 14 Apr. 1661 (Saturday) MURDEREXCEPTION Joe Dow (Witch) is killed by Nick White (Werewolf)
 15 Apr. 1661 (Sunday) Karl Brandon (Vampire) left the Village.
 16 Apr. 1661 (Monday) Van Helsing (Witch) appeared in the Village.

Подсказки
Полнолуние - каждые 28 суток. Знаете дату одного - знаете дату всех
Используйте ООП и задача окажется не такой громоздкой
Всего 10 имен + 10 фамилий = 100 вариаций.

Бонусы
За дополнительные классы персонажей.
За фильтрацию при выводе скучных "Everybody sleeps. Nothing is happening"
За учет случаев: Ведьма встречает Оборотня в полнолуние 31 октября.
За юнит тесты к некоторым (или всем!) методам.

Над реализацией стоит подумать.
Поэтому следующее ДЗ будет относительно небольшой надстройкой над кодом этого ДЗ. Можете сдать их вместе.

//*************************************************************************//
HW9
К предыдущему заданию добавьте фичу сохранения истории деревни в файл
Добавьте также возможность чтения истории деревни из файла.

Примечание!
Программа с абсолютными путями заработает, но только на вашем компьютере.
Для ДЗ этого, в целом, достаточно.
Но если Вы хотите, чтобы код сразу запускался, скажем, на машине наставника воспользуйтесь относительными путями

// В этой папке запускается ваше приложение
System.out.println( System.getProperty("user.dir")); // Пускай => C:\\Mike\project

// Если путь к вашему файлу C:\\Mike\project\files\history.txt, то относительный путь

Path path = Path.of("files/history.txt");

// Обратите внимание на разделитель между файлом и директорией == "/"!
// Этот разделитель платформонезависим
// (т.е. будет работать и на Linux/MacOs и на Windows)

// Вы также можете узнать какой в вашей ОС и файловой системе разделитель вот так

System.out.println(Files.separator)

// Наконец перед использованием превратите относительный путь в полный
path = path.toAbsolutePath();

// И вперед!
Files.createFile(path);
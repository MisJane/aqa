package Village;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Village village = new Village();
        try {
            village.lifeCircle();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MurderException e) {
            e.printStackTrace();
        }

        Scanner scn = new Scanner(new FileInputStream("The Village History.txt"));
        while (scn.hasNextLine()) {
            System.out.println(scn.nextLine());
        }

    }
}

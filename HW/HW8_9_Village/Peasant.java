package Village;

public class Peasant extends Citizen {

    public Peasant(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        race = Race.PEASANT;
    }
}


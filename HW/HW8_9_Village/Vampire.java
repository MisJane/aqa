package Village;

public class Vampire extends Citizen {

    public Vampire(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        race = Race.VAMPIRE;
    }
}


package Village;

import com.github.javafaker.Faker;

import java.io.IOException;
import java.util.*;

public class Village {

    private GregorianCalendar startLife = new GregorianCalendar(1650, Calendar.JANUARY, 1);
    private GregorianCalendar endLife = new GregorianCalendar(1750, Calendar.DECEMBER, 31);

    private List<Calendar> moonDays = new ArrayList<>();

    public WriteToFile write = new WriteToFile();

    private int people = 50;
    Random rand = new Random();
    private List<Citizen> citizensList = new ArrayList<>();

    private Citizen getFullRace(int fullRace) {
        Citizen citizen = null;
        Faker faker = new Faker();
        switch (fullRace) {
            case 0:
                citizen = new Vampire(faker.name().firstName(), faker.name().lastName());
                break;
            case 1:
                citizen = new Witch(faker.name().firstName(), faker.name().lastName());
                break;
            case 2:
                citizen = new Werewolf(faker.name().firstName(), faker.name().lastName());
                break;
            case 3:
                citizen = new Peasant(faker.name().firstName(), faker.name().lastName());
                break;
        }
        return citizen;
    }

    public void createCitizen() {
        for (int i = 0; i < people; i++) {
            citizensList.add(getFullRace(rand.nextInt(4)));
        }
    }

    public void visit(Citizen citizenOne, Citizen citizenTwo, Calendar calendar) throws MurderException, DawnException, IOException {
        if (!(citizenOne.getRace().equals(Race.WITCH)) && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY
                && calendar.get(Calendar.DAY_OF_MONTH) == 13 || calendar.get(Calendar.MONTH) == Calendar.OCTOBER &&
                calendar.get(Calendar.DAY_OF_MONTH) == 31) {

            if (citizenTwo.getRace().equals(Race.WITCH)) {
                citizensList.remove(citizenOne);
            }

            throw new MurderException("MurderException " + citizenTwo + " was killed by the witch " + citizenOne);
        }

        if (citizenOne.getRace().equals(Race.VAMPIRE) && ((citizenTwo.getRace().equals(Race.WITCH) ||
                citizenTwo.getRace().equals(Race.PEASANT)))) {
            if (5 > rand.nextInt(100)) {
                citizenTwo.setRace(Race.VAMPIRE);
                write.writeToFileLn(citizenTwo + " became a vampire");
                return;
            }

            //5% Вызывается MurderException и житель исчезает из деревни
            if (citizenOne.getRace().equals(Race.VAMPIRE) && !citizenTwo.getRace().equals(Race.VAMPIRE)
                    && 5 > rand.nextInt(100)) {

                citizensList.remove(citizenTwo);
                throw new MurderException("The citizen " + citizenTwo + " is dead ");
            }

            //1% Вызывается DawnException и Vampire исчезает из деревни
            if (citizenOne.getRace().equals(Race.VAMPIRE) && !citizenTwo.getRace().equals(Race.VAMPIRE)
                    && 1 == rand.nextInt(100)) {
                citizensList.remove(citizenTwo);
                throw new DawnException(" The vampire " + citizenOne + " left the village ");
            }

            //Любой житель встречается с Werewolf в полнолуние, Вызывается MurderException и житель исчезает из деревни
            if (startLife.getTime().equals(moonDays) && (citizenOne.getRace().equals(Race.WEREWOLF))) {
                citizenTwo = citizensList.get(rand.nextInt(citizensList.size()));
                citizensList.remove(citizenTwo);
                throw new MurderException("MURDEREXCEPTION " + citizenOne + " ate  " + citizenTwo);
            }
        }
    }

    public void getMoonDay() {
        GregorianCalendar firstMoonDay = new GregorianCalendar(1650, Calendar.JANUARY, 28);
        while (firstMoonDay.before(endLife)) {
            firstMoonDay.add(Calendar.DAY_OF_MONTH, 28);
            GregorianCalendar newCal = new GregorianCalendar();
            newCal.setTime(firstMoonDay.getTime());
            moonDays.add(newCal);
        }
    }

    public void lifeCircle() throws IOException, MurderException {
        createCitizen();
        while (startLife.before(endLife)) {
            int action = rand.nextInt(4);

            write.writeToFile(" \n" + "The day is " + startLife.getTime() +
                    " And the count of citizens is " + citizensList.size() + " \n" + "Today ");
            startLife.add(Calendar.DATE, 1);

            switch (action) {
                case 0:
                    Citizen citizen = getFullRace(rand.nextInt(4));
                    citizensList.add(citizen);
                    write.writeToFileLn(citizen + "appeared to the village ");
                    break;

                case 1:
                    if (!citizensList.isEmpty()) {
                        write.writeToFileLn(citizensList.remove((rand.nextInt(citizensList.size()))) + "left the village ");
                    } else {
                        write.writeToFileLn("The end");
                    }
                    break;

                case 2:
                    if (!citizensList.isEmpty()) {
                        Citizen citizenOne = citizensList.get(rand.nextInt(citizensList.size()));
                        Citizen citizenTwo = citizensList.get(rand.nextInt(citizensList.size()));
                        write.writeToFile("citizen " + citizenOne + " drinks tea with " + citizenTwo + " \n");
                        try {
                            visit(citizenOne, citizenTwo, startLife);
                        } catch (MurderException murderException) {
                            write.writeToFileLn("MURDEREXCEPTION " + citizenTwo + " was killed by " + citizenOne);
                        } catch (DawnException exception) {
                            write.writeToFileLn("DAWNEXCEPTION " + citizenOne + " was killed by " + citizenTwo);
                        }
                        if (citizenOne.getRace().equals(Race.VAMPIRE)) {
                            if (citizenTwo.getRace().equals(Race.PEASANT) || citizenTwo.getRace().equals(Race.WITCH)) {
                                citizensList.remove(citizenTwo);
                                write.writeToFileLn(citizenTwo + " became a vampire and left the village ");
                            }
                        }
                        if (citizenTwo.getRace().equals(Race.VAMPIRE)) {
                            if (citizenOne.getRace().equals(Race.PEASANT) || citizenOne.getRace().equals(Race.WITCH)) {
                                citizensList.remove(citizenOne);
                                write.writeToFileLn(citizenTwo + " left the village ");
                            }
                        }
                    } else {
                        write.writeToFileLn("The end..");
                    }
                    break;

                case 3:
                    write.writeToFileLn("Nothing is happening. Everybody asleep.");
                    break;
            }
        }
    }
}

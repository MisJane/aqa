package Village;

public class Werewolf extends Citizen {

    public Werewolf(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        race = Race.WEREWOLF;
    }
}

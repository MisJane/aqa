package Village;

public class Witch extends Citizen {

    public Witch(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        race = Race.WITCH;
    }
}


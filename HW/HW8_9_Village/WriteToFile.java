package Village;

import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {
    private FileWriter file;
   // File filePath = new File("Village");

    public WriteToFile() {
        try {
      //      file = new FileWriter(filePath + "\\The Village History.txt");
            file = new FileWriter("The Village History.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeToFile(String history) throws IOException {
        file.write(history);
    }

    public void writeToFileLn(String history) throws IOException {
        file.write(history + "\n");
    }
}


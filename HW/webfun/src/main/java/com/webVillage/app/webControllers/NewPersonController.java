package com.webVillage.app.webControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NewPersonController {
    @GetMapping(path = "/person/new")
    public String getPerson() {
        return "newPerson";
    }
}

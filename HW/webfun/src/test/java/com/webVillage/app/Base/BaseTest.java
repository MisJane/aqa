package com.webVillage.app.Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class BaseTest {
    protected static WebDriver driver;
    private static final int TIMEOUT = 15;

    @BeforeAll
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        //System.setProperty("webdriver.chrome.driver", "C:/PROJects/Java/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        //если хотим скрыть браузер
        //  chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }

    @AfterAll
    public static void tearDown() {
        /*Allure.getLifecycle().addAttachment(
                "screenshot", "image/png", "png",
                ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)
        );*/
        //закомментировала - пока пишу тест, подают тесты ибо пусто))

        driver.close();
        driver.quit();
    }

    protected WebElement findXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public static void switchTo(int window) {
        Set<String> tabs = driver.getWindowHandles();
        ArrayList<String> tabsArray = new ArrayList<>(tabs);
        driver.switchTo().window(tabsArray.get(window));
    }

    public static void switchTo(String pagePath) {
        Set<String> tabs = driver.getWindowHandles();
        for (String tab : tabs) {
            driver.switchTo().window(tab);
            if (driver.getCurrentUrl().startsWith(pagePath)) {
                return;
            }
        }
        throw new RuntimeException("Вкладка не найдена");
    }

    public static boolean checkCookieContains(String key, String value) {
        return driver.manage().getCookieNamed(key).getValue().contains(value);
    }

    public static boolean checkCookieEquals(String key, String value) {
        return driver.manage().getCookieNamed(key).getValue().equals(value);
    }


}

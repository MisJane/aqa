package com.webVillage.app;

import com.webVillage.app.Base.BaseTest;
import com.webVillage.app.page.MainPage;
import com.webVillage.app.page.PersonPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.io.IOException;

public class JTest extends BaseTest {
    @Test
    public void JTest() throws InterruptedException {
        driver.get("http://localhost:8080/person/new");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String firstName = "Test";
        String lastName = "Testov";
        String age = "33";
        String occupation = "DOCTOR";
        String gender = "MALE";

        findXpath("//input[@id='firstName']").sendKeys(firstName);
        driver.findElement(By.id("secondName")).sendKeys(lastName);
        driver.findElement(By.cssSelector("#age")).sendKeys(age);
        driver.findElement(By.id("occupation")).click();
        findXpath(String.format("//option[@value='%s']", occupation)).click();
        driver.findElement(By.id("gender")).click();
        findXpath(String.format("//option[@value='%s']", gender)).click();
        driver.findElement(By.xpath("//*[@id=\"backButton\"]")).click();

        driver.get("http://localhost:8080/person/1");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"deleteButton\"]")).click();
        driver.switchTo().alert().accept();

        Thread.sleep(2000);
        driver.get("http://localhost:8080");
       /* String currURL = driver.getCurrentUrl();
        Assertions.assertEquals(currURL, "http://localhost:8080/");*/
        Thread.sleep(2000);
        driver.get("http://localhost:8080/person/1");
        Thread.sleep(5000);
        Assertions.assertEquals(driver.findElement(By.xpath("//*[@id=\"nameLoaded\"]")).getText(), "NAME: undefined undefined");
    }

}

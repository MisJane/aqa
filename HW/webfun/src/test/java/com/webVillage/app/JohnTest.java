package com.webVillage.app;

import com.webVillage.app.Base.BaseTest;
import com.webVillage.app.page.MainPage;
import com.webVillage.app.page.PersonPage;
import io.qameta.allure.Description;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.Locale;
import java.util.Set;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class JohnTest extends BaseTest {
    @Test
    public void checkNewTabOpened() {
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage();
        PersonPage personPage = mainPage.openJohn();
        personPage.switchTo();
        String johnNameSplit = personPage.getPersonName().split(": ")[1];
        Assertions.assertEquals(johnNameSplit, "John Dow");
    }

    @Test
    public void checkCookies() {
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage();
        boolean isCookieExist = checkCookieEquals("user", "Weyland-Yutani");
        Assertions.assertTrue(isCookieExist);
    }

    @Test
    @Description("Проверка на удаление John Dow")
    public void deleteJohn() throws InterruptedException {
        driver.get("http://localhost:8080/person/new");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String firstName = "Selenium";
        String lastName = "Seleniumov";
        String age = "33";
        String occupation = "DOCTOR";
        String gender = "MALE";

        //создаем нового пользователя, запоминаем id, сохраняем
        findXpath("//input[@id='firstName']").sendKeys(firstName);
        driver.findElement(By.id("secondName")).sendKeys(lastName);
        driver.findElement(By.cssSelector("#age")).sendKeys(age);
        driver.findElement(By.id("occupation")).click();
        findXpath(String.format("//option[@value='%s']", occupation)).click();
        driver.findElement(By.id("gender")).click();
        findXpath(String.format("//option[@value='%s']", gender)).click();
       /* String idURL = (String) js.executeScript("return document.URL");
        String idNumber = idURL.replaceAll("http://localhost:8080/person/", "");*/
        driver.findElement(By.xpath("//*[@id=\"backButton\"]")).click();

        //удаляем John Dow
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage();
        PersonPage personPage = mainPage.openJohn();
        personPage.switchTo();
        Thread.sleep(5000); //- см.BaseTest implicitlyWait
        driver.findElement(By.xpath("//*[@id=\"deleteButton\"]")).click();
        driver.switchTo().alert().accept();

        //Oleg said to check the current URL. if it is the main page, then ok.
        //потому что баг в приложении (невозможно удалить Джона)
        Thread.sleep(1000);
        String currURL = driver.getCurrentUrl();
        Assertions.assertEquals(currURL, "http://localhost:8080/");

    }

 /*   public boolean elementIsNotPresent(String xpath){
        return driver.findElements(By.xpath(String.valueOf($(By.xpath(xpath))))).isEmpty();

    }

    public static boolean isPresentAndDisplayed(final WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }*/

    @Test
    public void popupTest() {
        driver.get("http://localhost:8080/person/1");
        Set<String> oldWindow = driver.getWindowHandles();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.querySelector(\"#popupButton\").click()");
        Set<String> newWindow = driver.getWindowHandles();
        newWindow.removeAll(oldWindow);
        String newWindowHandle = newWindow.iterator().next();
        driver.switchTo().window(newWindowHandle);
        Assertions.assertEquals(driver.findElement(By.xpath("//body/h3[1]")).getText(), "Hello, World!");
        Assertions.assertEquals(driver.findElement(By.xpath("//h3[2]")).getText(),"Find me!");
        Assertions.assertEquals(driver.findElement(By.xpath("//h3[@class='header']")).getText(),"And me!");
        Assertions.assertEquals(driver.findElement(By.xpath("//h3[@class='header'][2]")).getText(),"NOW me!");
        Assertions.assertEquals(driver.findElement(By.xpath("//h3[@class='header yellow']")).getText(),"Do not forget about me");
        Assertions.assertEquals(driver.findElement(By.xpath("//h3[@class='header'][@thestyle='red']")).getText(),"It is simple");
        Assertions.assertEquals(driver.findElement(By.cssSelector("h3.kitty-cat.abracadabra")).getText(),"My turn");
        Assertions.assertEquals(driver.findElement(By.cssSelector("h3.bow-wow-dog")).getText(),"Where am I...");
        Assertions.assertEquals(driver.findElement(By.cssSelector("h3.kitty-cat:not(.abracadabra)")).getText(),"What about me");
        Assertions.assertEquals(driver.findElement(By.cssSelector("div#good h3")).getText(),"I am hiding");
        Assertions.assertEquals(driver.findElement(By.cssSelector("div#bad h3")).getText(),"U cannot get me!");
        Assertions.assertEquals(driver.findElement(By.cssSelector("div#ugly h3")).getText(),"Where is Blondie again?");
        Assertions.assertEquals(driver.findElement(By.cssSelector("p#not_me")).getText(),"Find my daddy please.");
        Assertions.assertEquals(driver.findElement(By.cssSelector("p#iAmLost")).getText(),"Look for my granny!");
    }
}


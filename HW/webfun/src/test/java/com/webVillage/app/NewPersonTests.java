/*
package com.webVillage.app;

import com.webVillage.app.Base.BaseTest;
//import com.webVillage.app.Base.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.Locale;

@ExtendWith(TestListener.class)
public class NewPersonTests extends BaseTest {

    @Test
    @Owner("AQA")
    @Description("Создается новый пользователь. Проверяются все заполненные поля.")
    public void addNewPersonTest() {
        driver.get("http://localhost:8080/person/new");
        String firstName = "Selenium";
        String lastName = "Seleniumov";
        String age = "33";
        String occupation = "DOCTOR";
        String gender = "MALE";

        //driver.findElement(By.xpath("//*[@id='firstName']")).sendKeys(firstName);
        //driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys(firstName);
        findXpath("//input[@id='firstName']").sendKeys(firstName);

        driver.findElement(By.id("secondName")).sendKeys(lastName);
        driver.findElement(By.cssSelector("#age")).sendKeys(age);

        driver.findElement(By.id("occupation")).click();
        driver.findElement(By.xpath("//option[@value='DOCTOR']")).click();

        driver.findElement(By.id("gender")).click();
        //driver.findElement(By.xpath(String.format( "//option[@value='%s']", gender))).click();
        findXpath(String.format("//option[@value='%s']", gender)).click();

        driver.findElement(By.xpath("//*[@id=\"doIt\"]")).click();
        //driver.findElement(By.xpath("//button[@id='doIt']")).click();

        String fullName = firstName + " " + lastName;
        checkRegistration(fullName, occupation.toLowerCase(Locale.ROOT), age);
        //checkRegistration(fullName, occupation, age);
    }

    @Test
    @Description("создание нового пользователя, проверка заполненных сохраненных данных. с приминением js")
    public void jsTest() {
        driver.get("http://localhost:8080/person/new");
        String firstName = "Selenium";
        String lastName = "Seleniumov";
        String age = "33";
        String occupation = "DOCTOR";
        String gender = "MALE";
        WebElement name = findXpath("//*[@id='firstName']");

        //Selenide.executeJavascript(script)
      */
/*   //приведение типа и присваиваем к типу driver
       JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", name);
        js.executeScript("arguments[0].value='George'", name);
        String title = (String) js.executeScript("return document.URL");
        System.out.println("here is a title-URL: " + title);*//*


        //else
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(String.format("document.querySelector(\"#firstName\").value='%s'", firstName));
        js.executeScript(String.format("document.querySelector(\"#secondName\").value='%s'", lastName));
        js.executeScript(String.format("document.querySelector(\"#age\").value='%s'", age));
        js.executeScript("document.querySelector(\"#occupation > option:nth-child(4)\").selected=true");
        js.executeScript("document.querySelector(\"#gender > option:nth-child(1)\").selected=true");

        //пример ожидания пока не произойдет действие..
        //new WebDriverWait(driver).until(ExpectedConditions.jsReturnsValue(..));
        js.executeScript("document.querySelector(\"#doIt\").click()");

        String fullName = firstName + " " + lastName;
        checkRegistration(fullName, occupation.toLowerCase(Locale.ROOT), age);
    }

    private void checkRegistrationJs(JavascriptExecutor js, String fullName, String occupation, String age) {
        String nameText = (String) js.executeScript("return document.querySelector(\"#nameLoaded\").textContent").toString();
        String occupationText = js.executeScript("return document.querySelector(\"#nameLoaded\").innerHTML").toString();
        String ageText = js.executeScript("return document.querySelector(\"#age\").innerText").toString();
        //см видео про приведение Object number = js.executeScript("return parseInt(document.querySelector(\"#number\").getAttribute(\"value\"))");
        //Long number = (Long) js.executeScript("return parseInt(document.querySelector(\"#number\").getAttribute(\"value\"))");

        Assertions.assertEquals(nameText, "NAME: " + fullName);
        Assertions.assertEquals(occupationText, "OCCUPATION: " + occupation);
        Assertions.assertEquals(ageText, "AGE: " + age);
    }

    private void checkRegistration(String fullName, String occupation, String age) {
        String nameText = driver.findElement(By.id("nameLoaded")).getText();
        //или можно заменить NAME на "" тогда assertEquals(nameText, fullName)
        // String nameText = driver.findElement(By.id("nameLoaded")).getText().replaceAll("NAME", "");

        //или ещё можно собрать строку для assert так: "NAME: " + fullname

        String occupationText = driver.findElement(By.id("occupation")).getText();
        String ageText = driver.findElement(By.id("age")).getText();

        Assertions.assertTrue(nameText.contains(fullName));
        Assertions.assertTrue(occupationText.contains(occupation));
        Assertions.assertTrue(ageText.contains(age));
    }

}
*/

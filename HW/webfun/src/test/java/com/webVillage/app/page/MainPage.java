package com.webVillage.app.page;

import com.webVillage.app.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    {
        pagePath = START_PAGE + "/";
    }

    @FindBy(xpath = "//a[@href='/person/1']")
    private WebElement johnDowButton;

    public MainPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PersonPage openJohn(){
        johnDowButton.click();
        return new PersonPage(driver);
    }
}


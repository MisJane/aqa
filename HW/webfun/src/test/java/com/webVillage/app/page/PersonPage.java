package com.webVillage.app.page;

import com.webVillage.app.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonPage extends BasePage {
    {
        pagePath = START_PAGE + "/person/";
    }

    @FindBy(id = "nameLoaded")
    private WebElement name;

    public PersonPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getPersonName(){
        return name.getText();
    }
}

